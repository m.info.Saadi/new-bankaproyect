import Vue from "vue";
import App from "../src/App.vue";
import router from "./routes";
import api from "./api";

Vue.use(api);
Vue.config.productionTip = false;

new Vue({
  render: (h) => h(App),
  router,
}).$mount("#app");
