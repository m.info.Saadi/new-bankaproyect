import { shallowMount } from "@vue/test-utils";
import AddMovementPage from "@/components/AddAmountPage/AddMovementPage.vue";

const mockCategoryList = [
  {
    id_category: "01",
    name_category: "ropa",
  },
  {
    id_category: "02",
    name_category: "comida",
  },
];
function mountPage() {
  const mockApiCallListCategory = jest.fn();
  console.log(mockCategoryList);
  mockApiCallListCategory.mockReturnValue(mockCategoryList);
  return shallowMount(AddMovementPage, {
    methods: {
      ...AddMovementPage.methods,
      displayListCategory: mockApiCallListCategory,
    },
  });
}

test.skip("Comporobamos que nos llega una Lista de Categorías en una llamada a la API", () => {
  const wrapper = mountPage();

  expect(wrapper.vm.categoryList).toEqual(mockCategoryList);
});
